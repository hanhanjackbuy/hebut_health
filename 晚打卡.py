#coding:utf-8
import json
import requests

def daka():
    headers = {
        "Host": "yqfw.hebut.edu.cn",
        "Connection": "keep-alive",
        "Content-Length": "12429",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat",
        "content-type": "application/json",
        "token": "",
        "Referer": "https://servicewechat.com/wxd3bdddce3e105289/65/page-frame.html",
        "Accept-Encoding": "gzip, deflate, br"
        }
    body = {"isIos":False,"addressMark":True,"address":"河北工业大学北辰校区东区八宿舍","bodyTemp":36.3,"city":"天津市","district":"北辰区","province":"天津市","lat":39.241719,"lng":117.058624}

    r = requests.post(
        "https://yqfw.hebut.edu.cn/api/hm/night/add", data=json.dumps(body), headers=headers).json()
    return r

if __name__ == "__main__":
    print(daka())