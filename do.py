#coding:utf-8
import json
import requests
import smtplib
from email.mime.text import MIMEText
from email.utils import formataddr

from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.events import EVENT_JOB_EXECUTED, EVENT_JOB_ERROR
import logging
import os
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='daka-log.txt',
                    filemode='a')


def yj(subject, content=""):
    # 配置发信邮箱，password是授权码
    host = "smtphz.qiye.163.com"
    user = "*@stu.hebut.edu.cn"
    password = "*"

    # 收件人常量化了，在此不同于常规的邮件发送函数
    head = str("打卡通知")
    subject = str(subject)
    content = str(content)
    msg = MIMEText(content, "plain", "utf-8")
    msg["subject"] = subject
    msg["from"] = formataddr([head, "notice@notice.com"])
    msg["to"] = ",".join(["*@qq.com"])
    asmtp = smtplib.SMTP()
    asmtp.connect(host, port="25")
    asmtp.login(user, password)
    asmtp.sendmail(user, ["*@qq.com"], str(msg))
    asmtp.quit()



def daka(t):
    # t为字符串格式 "0","1","2"为早中晚
    headers = {
        "Host": "yqfw.hebut.edu.cn",
        "Connection": "keep-alive",
        "Content-Length": "15127",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36 MicroMessenger/7.0.9.501 NetType/WIFI MiniProgramEnv/Windows WindowsWechat",
        "content-type": "application/json",
        "token": "*",
        "Referer": "https://servicewechat.com/wxd3bdddce3e105289/56/page-frame.html",
        "Accept-Encoding": "gzip, deflate, br"
        }
    body = {"isIos":False,"showInfo":["faculty","profession","grade","clazz","realName","username"],"addressMark":True,"ifShow":False,"isShowOther":False,"propsConfig":{"id":"dictId","name":"dictName","checked":"selectFlag"},"isChangeBt":True,"fillType":t,"bodyTemp":"*","address":"*","symptomFlag":"0","symptoms":[{"id":"0"}],"otherSymptom":"","dateIndex":0,"fillType2":t,"fillTime":None,"province":"*","city":"*","district":"*"}

    r = requests.post(
        "https://yqfw.hebut.edu.cn/api/hm/daily/add", data=json.dumps(body), headers=headers).json()
    return r

def do(t):
    r = daka(t)
    if r == {'code': 1, 'message': 'SUCCESS', 'data': ''}:
        yj("打卡成功")
    else:
        yj("打卡失败",r['message'])


def my_listener(event):
    if event.exception:
        yj("打卡系统奔溃了！")
    else:
        print ('任务照常运行...')


scheduler = BlockingScheduler()
scheduler.add_job(do, args=('0'), trigger='cron', hour=7)
scheduler.add_job(do, args=('1'), trigger='cron', hour=11)
scheduler.add_job(do, args=('2'), trigger='cron', hour=15)


scheduler.add_listener(my_listener, EVENT_JOB_EXECUTED | EVENT_JOB_ERROR)
scheduler._logger = logging
print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))

try:
    scheduler.start()
except (KeyboardInterrupt, SystemExit):
    pass 